import React, { useEffect, useState } from "react";
import { supabase } from "./supabaseClient";
import "./App.css";

// App.js

const App = () => {
  const [user, setUser] = useState(null);

  useEffect(() => {
    // Check for active session on component mount
    const session = supabase.auth.session();
    setUser(session?.user ?? null);

    // Listen for changes in auth state
    const { data: authListener } = supabase.auth.onAuthStateChange(
      (event, session) => {
        setUser(session?.user ?? null);
      }
    );

    return () => {
      authListener.unsubscribe();
    };
  }, []);

  const signInWithGoogle = async () => {
    const { error } = await supabase.auth.signIn({
      provider: "google",
    });

    if (error) {
      console.error("Error signing in with Google:", error);
    }
  };

  const signOut = async () => {
    const { error } = await supabase.auth.signOut();
    if (error) {
      console.error("Error signing out:", error);
    }
  };

  return (
    <div>
      <h1>Supabase SSO with React</h1>
      {user ? (
        <div>
          <p>Welcome, {user.email}</p>
          <button onClick={signOut}>Sign Out</button>
        </div>
      ) : (
        <button onClick={signInWithGoogle}>Sign In with Google</button>
      )}
    </div>
  );
};

export default App;
