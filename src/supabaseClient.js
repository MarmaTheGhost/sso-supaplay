import { createClient } from "@supabase/supabase-js";

const supabaseUrl = "http://localhost:8000";
const supabaseAnonKey = process.env.ANON_KEY;

export const supabase = createClient(supabaseUrl, supabaseAnonKey);
